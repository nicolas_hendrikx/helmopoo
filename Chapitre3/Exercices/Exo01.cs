﻿using System;

namespace Exercices
{
	public class Exo01
	{
		public static string ENONCE = @"
            En C#, le programmeur peut affecter des valeurs par défaut aux paramètres des méthodes.
            Ce mécanisme évite au programmeur d'écrire de nouvelles surcharges. En effet,
            les paramètres pourvus d'une valeur par défaut deviennent optionnels. Lors d'un appel de méthode, le programmeur
            peut omettre de préciser une valeur pour ces paramètres, le runtime leur affectant alors la valeur par défaut.

            Supprimez les surcharges de la méthode Somme et utilisez des valeurs par défaut pour que l'appel
            à la méthode FaireUneSomme fonctionne.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        private static int Somme(int a, int b) 
        {
            throw new NotImplementedException(Enonce);
        }

        private static int Somme(int a, int b, int c)
        {
            throw new NotImplementedException(Enonce);
        }

        private static int Somme(int a, int b, int c, int d)
        {
            return a+b+c+d;
        }

        public static int FaireUneSomme() 
		{
            return Somme(0,6) + 
                Somme(6, 12, 18) + 
                Somme(1,2,2);
		}
            
	}
}

