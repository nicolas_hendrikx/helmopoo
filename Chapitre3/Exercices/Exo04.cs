﻿using System;
using System.Linq;

namespace Exercices
{
    public class Exo04
    {
        private const string ENONCE = @"
            Comme l'éllipse Java, le qualificatif params permet de transmettre un nombre variables
            d'arguments de même type et de les manipuler comme une collection.

            Comme les arguments optionnels, le qualificatif params permet d'éviter les surcharges.

            Inspirez-vous de la méthode Maximum pour transformer la méthode Somme et vous passez des surcharges.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static int Maximum(params int[] valeurs) 
        {
            //Cet appel est légal grâce à l'instruction using System.Linq; (cf.C# succintly - ch 7)
            return valeurs.Max(); 
        }

        public static int SommeDesMax() 
        {
            return Maximum(1,2) 
                + Maximum(10,20,30) 
                + Maximum(1,2,3,4,5,6,7,8,9,10);
        }

        private static int Somme(int a, int b) 
        {
            throw new NotImplementedException(Enonce);
        }

        private static int Somme(int a, int b, int c)
        {
            throw new NotImplementedException(Enonce);
        }

        private static int Somme(int a, int b, int c, int d)
        {
            return a+b+c+d;
        }

        public static int FaireUneSomme() 
        {
            return Somme(0,6) + 
                Somme(6, 12, 18) + 
                Somme(1,2,2);
        }
    }
}

