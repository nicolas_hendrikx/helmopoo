﻿using System;

namespace Exercices
{
    public class Exo03
    {
        private const string ENONCE = @"
            Les qualificatifs ref et out, appliqués à un paramètre, indique que ces derniers doivent être
            transmis, non pas par valeur, mais par référence (comme les objets). Conséquence directe,
            le programmeur transmettra uniquement des variables à ces méthodes, pas de valeurs littérales, 
            ni des appels de méthode. A la différence du qualificatif out, le qualificatif ref impose 
            que la variable référencée ait été initialisée au préalable
         
            Ces qualificatifs peuvent être utiles si vous souhaitez retourner plusieurs valeurs pour un appel de méthode.
            Cependant, l'utilisation de classe et/ou de structures de données sont souvent de meilleures alternatives.

            Modifiez la méthode Methode afin qu'elle utilise l'opérateur + pour doubler la valeur de i et pour concaténer
            s1 à lui-même et s2 à lui-même. Modifiez éventuellement l'appel à Method dans la méthode FaireUneString.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        static void Method(out int i, out string s1, out string s2)
        {
            i =  42;
            s1 = "réponses à tout";
            s2 = " autres réponses au reste"; 
        }

        public static string FaireUneString() 
        {
            int a = 1;
            string string1 = "Bi";
            string string2 = "Lo";

            Method(out a, out string1, out string2);

            return $"J'ai {a} {string1} et {a} {string2}";
        }

    }
}

