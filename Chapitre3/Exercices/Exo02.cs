﻿using System;

namespace Exercices
{
    public class Exo02
    {
        private const string ENONCE = @"
            Le C# offre une alternative aux appels de méthodes classiques tels qu'on peut
            les retrouver en Java. Avec cette alternative, le programmeur nomme explicitement
            les paramètres et leur affecte une expression.

            Grâce à ce mécanisme, le programmeur peut appeler une méthode sans tenir compte de l'ordre des arguments.

            Inspirez-vous de l'exemple pour appeler la méthode Formater en nommant explicitement les paramètres.
            Essayez également de changer l'ordre des paramètres.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');


        public static double Diviser(int dividende, int diviseur) 
        {
            return (double)dividende / diviseur;
        }
          
        public static double UnExemple() 
        {
            return Diviser(dividende: 84, diviseur: 2) * Diviser(diviseur: 42, dividende: 42);
        }
         
        public static string Formater(int annee, int mois, int jour) 
        {
            return $"{jour,02}/{mois,02}/{annee,04}";
        }

        public static string Noel() 
        {
            //TODO: Appeler Formater
            throw new NotImplementedException(Enonce);
        }
    }
}

