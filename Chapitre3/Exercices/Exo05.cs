﻿using System;
using System.Collections.Generic;

namespace Exercices
{
    public class Exo05
    {
        private const string ENONCE = @"
            Les méthodes génératrices retourne une énumération de valeurs, utilisable notamment avec une boucle foreach.
            A cette fin, le programmeur utilise l'instruction yield return <valeur> où valeur correspond à un élément
            de l'énumération.
            
            Contrairement à une instruction return classique, yield return interrompt l'appel de méthode et rend la main 
            à la méthode appelante, jusqu'à ce que la valeur suivante soit requise. La méthode disparait de la pile
            lorsque toutes les valeurs de l'énumération ont été consommées.

            Utilisez l'instruction yield return pour générer une énumération des N pairs.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public IEnumerable<int> LesCinqPremiersImpairs() 
        {
            yield return 1;
            yield return 3;
            yield return 5;
            yield return 7;
            yield return 9;
        }

        public static IEnumerable<int> LesNPremiersPairs(int n) 
        {
            throw new NotImplementedException(Enonce);
        }

    }
}

