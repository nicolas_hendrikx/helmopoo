﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo04
    {
        [Test]
        public void ValiderEstUnString()
        {
            Assert.That(Exercices.Exo04.FaireUneSomme(), Is.EqualTo(42), Exercices.Exo04.Enonce);
        }
    }
}

