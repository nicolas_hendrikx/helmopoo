﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo03
    {
        [Test]
        public void DeuxBibiEtUnLolo()
        {
            Assert.That(Exercices.Exo03.FaireUneString(), Is.EqualTo("J'ai 2 BiBi et 2 LoLo"), Exercices.Exo03.Enonce);
        }



    }
}

