﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo05
    {
        [TestCase(1,new int[] { 0 })]
        [TestCase(2,new int[] { 0,2 })]
        [TestCase(3,new int[] { 0,2,4 })]
        [TestCase(4,new int[] { 0,2,4,6 })]
        [TestCase(5,new int[] { 0,2,4,6,8 })]
        public void ValiderNPremiersPairs(int recu, int[] attendu)
        {
            //Is.EquivalentTo vérifie qu'une collection contient les mêmes éléments qu'une seconde.
            Assert.That(Exercices.Exo05.LesNPremiersPairs(recu), Is.EquivalentTo(attendu), Exercices.Exo05.Enonce);
        }
            
    }
}

