﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo02
    {
        [Test]
        public void NoelTombeLe24Decembre2016()
        {
            Assert.That(Exercices.Exo02.Noel(), Is.EqualTo("24/12/2016"));
        }
    }
}

