﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo01
    {
        [Test]
        public void FaireSommeRetourne42()
        {
            Assert.That(Exercices.Exo01.FaireUneSomme(), Is.EqualTo(42),
                Exercices.Exo01.Enonce);
        }
    }
}

