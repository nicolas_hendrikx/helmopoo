﻿using System;

namespace Exercices
{
    public class Exo03
    {
        private const string ENONCE = @"
            Le C# est un langage fortement typé : il est, par exemple, impossible d'y affecter implicitement un string à un int.
            
            Pour réaliser une telle opération, le développeut peut appeler les méthodes Parse proposées par chaque type primitif.
            Autre possibilité lorsque les types sont compatibles, utiliser l'opérateur de cast (<type>).

            Modifiez le corps des méthodes suivantes afin de retourner la valeur passée en argument dans le bon type.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static int EnEntier(string valeurEnString) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static float EnFloat(string valeurEnString)
        {
            throw new NotImplementedException(Enonce);
        }

        public static byte EnByte(long valeurEnLong)
        {
            throw new NotImplementedException();
        }

    }
}

