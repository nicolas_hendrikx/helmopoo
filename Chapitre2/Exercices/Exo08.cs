﻿using System;

namespace Exercices
{
    public class Exo08
    {
        private const string ENONCE = @"
            Le C# propose les mêmes structures de sélection qu'en Java.
            La structure switch...case...default existe également en C# et permet notamment
            de soumettre un string à plusieurs cas.
    
            Complétez la méthode Signe du Zodiaque pour qu'elle retourne l'indice du signe passé en paramètre.
            (Belier->0, ..., Poisson -> 12).
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static int SigneDuZodiaqueEnInt(string signe) 
        {
            throw new NotImplementedException(Enonce);
        }

    }
}

