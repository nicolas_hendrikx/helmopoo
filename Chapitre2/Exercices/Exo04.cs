﻿using System;

namespace Exercices
{
    public class Exo04
    {
        private const string ENONCE = @"
            Le C# offre plusieurs opérateurs pour calculer de nouvelles valeurs.

            La langage partage beaucoup d'opérateurs avec le Java.
            Parfois, ils portent des noms différents. Plus rarement, ils n'ont pas d'équivalent en Java.
            C'est le cas de l'opérateur x ?? y qui retourne l'élément référencé par x si x est non-null et y sinon.
            L'opérateur e is T retourne true si l'élément e est de type T.

            Complétez les méthodes à l'aide de ses deux opérateurs.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static bool EstUnString(object o) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static string StringOuParDefaut(string s) 
        {
            throw new NotImplementedException(Enonce);
        }
    }
}

