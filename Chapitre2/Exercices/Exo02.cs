﻿using System;

namespace Exercices
{
    public class Exo02
    {
        private const string ENONCE = @"
            Le C# propose plusieurs types primitifs numériques. Le programmeur doit suffixer une valeur littérale
            pour indiquer au compilateur son type, sous peine d'erreur.

            Dans chaque méthode, retournez une valeur littérale avec le bon suffixe.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');


          public static decimal EnDecimal() 
          {
            throw new NotImplementedException(Enonce);
          }
          
          public static uint EnNonSigne() 
          {
            throw new NotImplementedException(Enonce);
          }
         
    }
}

