﻿using System;

namespace Exercices
{
    public class Exo09
    {
        private const string ENONCE = @"
            Le C# propose les mêmes structures de sélection qu'en Java.
            Les structures de répétition sont identiques à Java à l'exception
            de la boucle foreach dont la syntaxe change (cf. Exo10).
    
            La méthode Explosion recoit un string et retourne tous les préfixes
            du string concaténés (code => ccocodcode).

            La méthode OccurrencesDesDeuxDerniers compte le nombre de fois que
            les deux dernièrs caractères du string passé en argument apparaissent...
            tout en ne tenant pas compte des deux derniers caractères.

            La méthode CompterEnYEtEnZ compte le nombre de mots d'un string
            se terminant par Y ou par Z.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static string Exploser(string str) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static int OccurrencesDesDeuxDerniers(string str) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static int CompterEnYEtEnZ(string str) 
        {
            throw new NotImplementedException(Enonce);
        }
    }
}

