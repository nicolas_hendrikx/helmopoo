﻿using System;

namespace Exercices
{
    public class Exo05
    {
        private const string ENONCE = @"
            Les exercices suivants manipulent les opérateurs arithmétiques et logique et l'opérateur ternaire de sélection (b?x:y) du C#.
            Ces derniers sont les mêmes qu'en Java.

            Les exercices sont inspirés de la série Warmup-1 disponibles sur le site de codingbat.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static bool MultipleDe3OuDe5(int n) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static int Diff21(int n) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static bool Dans1020(int val1, int val2)
        {
            throw new NotImplementedException(Enonce);
        }

        public static bool NegationComplique(int val1, int val2, bool unEtUnSeulNegatif) 
        {
            throw new NotImplementedException();
        }
            
    }
}

