﻿using System;

namespace Exercices
{
    public class Exo07
    {
        private const string ENONCE = @"
            Le C# propose les mêmes structures de sélection qu'en Java.
            Complétez les méthodes suivantes en utilisant des if...else...else if.

            La méthode ArrondirAuMultipleDeDix retourne la somme des trois valeurs arrondi 
            à un multiple de dix (10,20,etc.).

            La méthode BlackJack retourne l'entier le plus proche de 21 pour autant qu'il ne dépasse pas 21.
            Si les deux entiers dépassent 21, alors la méthode retourne 0.

            La méthode MaconnerMur retourne true si, étant donné nbrPetitesBriques de 1 dm et nbrGrandesBriques de 5 dm
            on peut maconner un mur de longueurCible dm.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static int ArrondirAuMultipleDeDix(int val1, int val2, int val3) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static int BlackJack(int main1, int main2)
        {
            throw new NotImplementedException(Enonce);
        }

        public static bool MaconnerMur(int nbrPetitesBriques, int nbrGrandesBriques, int longueurCible)
        {
            throw new NotImplementedException(Enonce);
        }

    }
}

