﻿using System;

namespace Exercices
{
    public class Exo10
    {
        private const string ENONCE = @"
            Le C# propose plusieurs types de collections.
            La plus simple est le tableau à une dimension qui s'utilise comme en Java.
            Le C# propose également une Liste, nommé List, qui s'utilise également comme en Java.

            La méthode EstDansToutesLesPaires retourne true si pour chaque paire 
            d'éléments consécutifs de tab, la paire contient l'élément recherche.

            La méthode Soit2Soit4 retourne true si le tableau contient soit deux fois
            consécutivement la valeur 2 soit deux fois consécutivement la valeur 4.

            La méthode EstDecoupable retourne true si il existe un indice tel que
            la somme des éléments allant de 0 à cet indice égale
            la somme des élements allant de cet indice +1 à la fin du tableau.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static bool EstDansToutesLesPaires(int[] tab, int elementRecherche) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static bool Soit2Soit4(int[] tab) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static bool EstDecoupable(int[] tab) 
        {
            throw new NotImplementedException(Enonce);
        }
    }
}

