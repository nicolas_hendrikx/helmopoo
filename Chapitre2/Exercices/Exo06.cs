﻿using System;

namespace Exercices
{
    public class Exo06
    {
        private const string ENONCE = @"
            Plusieurs techniques permettent de construire des string à partir d'autres donnnées.
            La plus simple consiste à utiliser la concaténation.

            Une bien meilleure approche consiste à utiliser la méthode Format proposée par la classe string.
            Cette dernière utilise des marqueurs pour indiquer où injecter les données. Parmi les avantages
            de la méthode Format, citons les possibilités accrues de mises en forme (nombre de colonnes occupées,
            alignement, mise en forme spécifique à un type, etc.).

            Depuis la version 6 du langage, le programmeur peut également utiliser l'interpolation de string.

            Utilisez la méthodes Format pour mettre en forme les données.
        ";

        public static string Enonce => ENONCE.Replace('\t','\0');

        public static string FormaterProduit(string nom, decimal prix) 
        {
            throw new NotImplementedException(Enonce);
        }

        public static string MettreEnPourcent(double valeur)
        {
            throw new NotImplementedException(Enonce);
        }

    }
}

