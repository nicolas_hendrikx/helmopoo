﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo09
    {
        [TestCase("Code","CCoCodCode")]
        [TestCase("abc","aababc")]
        [TestCase("ab","aab")]
        [TestCase("x","x")]
        [TestCase("fade","ffafadfade")]
        [TestCase("There","TThTheTherThere")]
        public void ValiderExploser(string str, string attendu)
        {
            Assert.That(Exercices.Exo09.Exploser(str), 
                Is.EqualTo(attendu), Exercices.Exo09.Enonce);   
        }

        [TestCase("axxxaaxx",2)]
        [TestCase("xxaxxaxxaxx",3)]
        [TestCase("xaxaxaxx",0)]
        [TestCase("xxxx",2)]
        [TestCase("h",0)]
        [TestCase("",0)]
        public void ValiderOccurrencesDesDeuxDerniers(string str, int attendu) 
        {
            Assert.That(Exercices.Exo09.OccurrencesDesDeuxDerniers(str),
                Is.EqualTo(attendu), Exercices.Exo09.Enonce);
        }

        [TestCase("day fez",2)]
        [TestCase("day:yak",1)]
        [TestCase("!!day--yaz!!",2)]
        [TestCase("yak zak",0)]
        [TestCase("DAY abc XYZ",2)]
        [TestCase("y2bz",2)]
        public void ValiderCompterEnYEtEnZ(string str, int attendu) 
        {
            Assert.That(Exercices.Exo09.CompterEnYEtEnZ(str),
                Is.EqualTo(attendu), Exercices.Exo09.Enonce);
        }
    }
}

