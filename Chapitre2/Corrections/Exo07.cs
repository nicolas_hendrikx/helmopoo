﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo07
    {
        [TestCase(16, 17, 18, 20)]
        [TestCase(12, 13, 14, 30)]
        [TestCase(6, 4, 4, 10)]
        [TestCase(23, 24, 25, 70)]
        [TestCase(25, 24, 25, 80)]
        [TestCase(0, 0, 1, 0)]
        public void ValiderMettreEnProduit(int val1, int val2, int val3, int attendu)
        {
            Assert.That(Exercices.Exo07.ArrondirAuMultipleDeDix(val1, val2, val3), 
                Is.EqualTo(attendu), Exercices.Exo07.Enonce);
        }

        [TestCase(19, 21, 21)]
        [TestCase(21, 19, 21)]
        [TestCase(19, 22, 19)]
        [TestCase(34, 33, 0)]
        [TestCase(16, 23, 16)]
        [TestCase(3, 4, 4)]
        public void ValiderBlackjack(int main1, int main2, int attendu)
        {
            Assert.That(Exercices.Exo07.BlackJack(main1, main2), 
                Is.EqualTo(attendu), Exercices.Exo07.Enonce);              
        }

        [TestCase(3, 1, 8, true)]
        [TestCase(3, 1, 9, false)]
        [TestCase(3, 2, 10, true)]
        [TestCase(6, 0, 11,false)]
        [TestCase(1, 4, 11,true)]
        [TestCase(0, 3, 10,true)]
        [TestCase(1, 4, 12,false)]
        public void ValiderMaconnerMur(int nbrPetitesBriques, int nbrGrandesBriques, int longueurMurCible, bool attendu)
        {
            Assert.That(Exercices.Exo07.MaconnerMur(nbrGrandesBriques, nbrPetitesBriques, longueurMurCible),
                Is.EqualTo(attendu), Exercices.Exo07.Enonce);
        }
    }
}

