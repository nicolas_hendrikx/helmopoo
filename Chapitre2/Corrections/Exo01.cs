﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo01
    {
        [Test]
        public void HelloWorld()
        {
            Assert.That(Exercices.Exo01.DireBonjour(), Is.EqualTo("Hello Wordl"),
                Exercices.Exo01.Enonce);
        }
    }
}

