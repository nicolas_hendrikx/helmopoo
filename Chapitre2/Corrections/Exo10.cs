﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo10
    {
        [TestCase(new int [] {2, 1, 2, 1}, 2,true)]
        [TestCase(new int [] {2, 1, 2, 3, 1}, 2,false)]
        [TestCase(new int [] {3, 1}, 3,true)]
        [TestCase(new int [] {3, 1}, 2,false)]
        [TestCase(new int [] {3}, 1,true)]
        [TestCase(new int [] {}, 1,true)]
        public void ValiderEstDansToutesLesPaires(int[] tab, int element, bool attendu)
        {
            Assert.That(Exercices.Exo10.EstDansToutesLesPaires(tab, element),
                Is.EqualTo(attendu), Exercices.Exo10.Enonce);
        }

        [TestCase(new int [] {3, 5, 9},false)]
        [TestCase(new int [] {1, 2, 3, 4, 4},true)]
        [TestCase(new int [] {2, 2, 3, 4},true)]
        [TestCase(new int [] {4, 4},true)]
        [TestCase(new int [] {2},false)]
        [TestCase(new int [] {},false)]
        public void ValiderSoit2Soit4(int[] tab, bool attendu) 
        {
            Assert.That(Exercices.Exo10.Soit2Soit4(tab), 
                Is.EqualTo(attendu), Exercices.Exo10.Enonce);
        }

        [TestCase(new int[] {10, 10},true)]
        [TestCase(new int[] {10, 0, 1, -1, 10},true)]
        [TestCase(new int[] {2, 3, 4, 1, 2},false)]
        [TestCase(new int[] {1, 2, 3, 1, 0, 2, 3},true)]
        [TestCase(new int[] {1, 2, 3, 1, 0, 1, 3},false)]
        [TestCase(new int[] {1},false)]
        public void ValiderEstDecoupable(int[] tab, bool attendu) 
        {
            Assert.That(Exercices.Exo10.EstDecoupable(tab), 
                Is.EqualTo(attendu), Exercices.Exo10.Enonce);
        }
    }
}

