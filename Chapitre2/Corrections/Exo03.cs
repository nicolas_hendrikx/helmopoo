﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo03
    {
        [Test]
        public void ConversionStringVersInt()
        {
            Assert.That(Exercices.Exo03.EnEntier("314"), Is.EqualTo(314), Exercices.Exo03.Enonce);
            Assert.That(Exercices.Exo03.EnEntier("42"), Is.EqualTo(42), Exercices.Exo03.Enonce);
        }

        [Test]
        public void ConversionStringVersFloat() 
        {
            Assert.That(Exercices.Exo03.EnFloat("3.14159"), Is.EqualTo(3.14159f).Within(0.000001f),Exercices.Exo03.Enonce);
        }

        [Test]
        public void ConversionLongVersByte()
        {
            Assert.That(Exercices.Exo03.EnByte(0L), Is.EqualTo(0), Exercices.Exo03.Enonce);
            Assert.That(Exercices.Exo03.EnByte(128L), Is.EqualTo(128), Exercices.Exo03.Enonce);

            //Les résultats suivants sont surprenants, mais parfaitement déterministe. Savez-vous pourquoi?
            Assert.That(Exercices.Exo03.EnByte(512L), Is.EqualTo(0), Exercices.Exo03.Enonce);
            Assert.That(Exercices.Exo03.EnByte(514L), Is.EqualTo(2), Exercices.Exo03.Enonce);
        }

    }
}

