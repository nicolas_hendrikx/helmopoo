﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo06
    {
        [TestCase("1234567", 12.99, "1234567   12,99 €")]
        [TestCase("1234567", 12.999, "1234567   13,00 €")]
        [TestCase("123", 2.999, "123       3,00 €")]
        public void ValiderMettreEnProduit(string nom, decimal prix, string attendu) 
        {
            Assert.That(Exercices.Exo06.FormaterProduit(nom, prix), Is.EqualTo(attendu), Exercices.Exo06.Enonce);
        }

        [TestCase(1,"100,00 %")]
        [TestCase(0.5,"50,00 %")]
        [TestCase(0.3333,"33,33 %")]
        public void ValiderMettreEnPourcent(double valeur, string attendu) 
        {
            Assert.That(Exercices.Exo06.MettreEnPourcent(valeur), Is.EqualTo(attendu), Exercices.Exo06.Enonce);
        }
    }
}

