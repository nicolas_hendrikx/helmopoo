﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo02
    {
        [Test]
        public void ValeurLitteraleDecimale()
        {
            Assert.That(Exercices.Exo02.EnDecimal(),Is.InstanceOf(typeof(decimal)));
        }

        [Test]
        public void ValeurLitteraleNonSigne() 
        {
            Assert.That(Exercices.Exo02.EnNonSigne(),Is.InstanceOf(typeof(uint)));
        }
    }
}

