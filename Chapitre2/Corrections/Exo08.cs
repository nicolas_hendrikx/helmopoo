﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo08
    {
        [TestCase("Bélier", 0)]
        [TestCase("bélier", 0)]
        [TestCase("LION", 4)]
        [TestCase("pOiSsOn", 11)]
        [TestCase("gémeaux", 2)]
        public void ValiderSigneDuZodiaque(string signe, int attendu) 
        {
            Assert.That(Exercices.Exo08.SigneDuZodiaqueEnInt(signe), Is.EqualTo(attendu), Exercices.Exo08.Enonce);
        }
    }
}

