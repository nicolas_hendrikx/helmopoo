﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo05
    {
        [TestCase(3,true)]
        [TestCase(5,true)]
        [TestCase(13,false)]
        [TestCase(15,true)]
        [TestCase(35,true)]
        [TestCase(101,false)]
        public void ValiderMultipleDe3OuDe5(int recu, bool attendu)
        {
            Assert.That(Exercices.Exo05.MultipleDe3OuDe5(recu), Is.EqualTo(attendu), Exercices.Exo05.Enonce);
        }

        [TestCase(12,99,true)]
        [TestCase(8,99,false)]
        [TestCase(9,21,false)]
        [TestCase(9,20,true)]
        [TestCase(10,21,true)]
        [TestCase(10,20,true)]
        [TestCase(9,9,true)]
        public void ValiderDans1020(int val1, int val2, bool attendu)
        {
            Assert.That(Exercices.Exo05.Dans1020(val1, val2), Is.EqualTo(attendu), Exercices.Exo05.Enonce);
        }

        [TestCase(19,2)]
        [TestCase(10,11)]
        [TestCase(21,0)]
        [TestCase(30,18)]
        [TestCase(-1,22)]
        [TestCase(50,58)]
        public void ValiderDiff21(int recu, int attendu)  
        {
            Assert.That(Exercices.Exo05.Diff21(recu), Is.EqualTo(attendu), Exercices.Exo05.Enonce);
        }

        [TestCase(1, 1, true, false)]
        [TestCase(1, 1, false, false)]
        [TestCase(-4, 5, true, true)]
        [TestCase(-5, -6, false, true)]
        [TestCase(-6, 6, true, true)]
        [TestCase(-6, 6, false, false)]
        [TestCase(-5, -6, true, false)]
        public void ValiderNegationComplique(int val1, int val2, bool unEtUnSeulNegatif, bool attendu) 
        {
            Assert.That(Exercices.Exo05.NegationComplique(val1,val2, unEtUnSeulNegatif), Is.EqualTo(attendu), Exercices.Exo05.Enonce);
        }
            
    }
}

