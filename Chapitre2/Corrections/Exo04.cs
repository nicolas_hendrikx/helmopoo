﻿using System;
using NUnit.Framework;

namespace Corrections
{
    [TestFixture]
    public class Exo04
    {
        [TestCase(null, false)]
        [TestCase("", true)]
        [TestCase("another", true)]
        [TestCase(42, false)]
        [TestCase("42", true)]
        public void ValiderEstUnString(object value, bool expectedResult)
        {
            Assert.That(Exercices.Exo04.EstUnString(value), Is.EqualTo(expectedResult), Exercices.Exo04.Enonce);
        }

        [TestCase(null, "")]
        [TestCase("A", "A")]
        [TestCase("B", "B")]
        public void ValiderStringOuParDefault(string donne, string attendu) 
        {
            Assert.That(Exercices.Exo04.StringOuParDefaut(donne), Is.EqualTo(attendu), Exercices.Exo04.Enonce);
        }
    }
}

